import numpy as np
import cv2
import csv

filepath = "D:/Deep_Learning/face_detection/DataSetImages"

excelpath = "../faceexp-comparison-data-train-public.csv"

def infer():
    with open(excelpath) as f:
        line = csv.reader(f, delimiter = ',')
        for l in line:
            li = l[0].split('/')[-1]
            print(filepath +"/" +li)
            img = cv2.imread(str(filepath) + "/" + str(li) + ".jpg")
            resized_h = 128
            resized_w = 256


            if img is None:
                pass
            else:

                img = cv2.resize(img, (resized_w, resized_h))
                row = len(img)
                col = len(img[0])
                top_left_col = int(float(l[1]) * col)
                top_left_row = int(float(l[3]) * row)
                bottom_right_col = int(float(l[2]) * col)
                bottom_right_row = int(float(l[4]) * row)

                cv2.rectangle(img, (top_left_col, top_left_row), (bottom_right_col, bottom_right_row), (0, 255, 0), 3)
                print(len(img))
                print(len(img[0]))
                cv2.imshow("img", img)
                k  = cv2.waitKey(1)
                if  k == 27:
                    break


if __name__ == "__main__":
    infer()